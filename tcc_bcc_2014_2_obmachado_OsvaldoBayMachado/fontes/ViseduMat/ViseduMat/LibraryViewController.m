//
//  LibraryViewController.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 11/16/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "LibraryViewController.h"

@interface LibraryViewController()

@property (nonatomic) EnumModo modo;

@end

@implementation LibraryViewController

- (id)initWithModo:(EnumModo)m
{
    if (self = [super init]) {
        self.modo = m;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.0f];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    UIButton *buttonManual = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonManual addTarget:self
               action:@selector(botaoManual:)
     forControlEvents:UIControlEventTouchUpInside];
    [buttonManual setTitle:@"MANUAL DO USUÁRIO" forState:UIControlStateNormal];
    buttonManual.frame = CGRectMake(10, 30, 200, 30);
    [self.view addSubview:buttonManual];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(botaoVoltar:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"RETORNAR" forState:UIControlStateNormal];
    button.frame = CGRectMake(10, 60, 200, 30);
    [self.view addSubview:button];
    
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 90, screenHeight, screenWidth)];
    scrollView.showsVerticalScrollIndicator=YES;
    scrollView.scrollEnabled=YES;
    scrollView.userInteractionEnabled=YES;
    [self.view addSubview:scrollView];
    

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(230, 0, 570, 700)];
    UIImage *wonImage;
    
    if (self.modo == MODO_2D) {
         wonImage = [UIImage imageNamed:@"biblioteca2D.png"];
    }
    else
    {
        wonImage = [UIImage imageNamed:@"biblioteca3D.png"];
    }
    
    
    [imageView setImage:wonImage];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(230, 710, 570, 376)];
    UIImage *wonImage2 = [UIImage imageNamed:@"operadoresFuncoes.png"];
    [imageView2 setImage:wonImage2];
    
    [scrollView addSubview:imageView];
    [scrollView addSubview:imageView2];
    [scrollView setContentSize:CGSizeMake(570, 1200)];
    
    
}

-(void)botaoVoltar:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion: nil];
}

-(void)botaoManual:(UIButton *)sender
{
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"ManualUsuario" withExtension:@"pdf"];
    if (URL)
    {
        self.controller = [UIDocumentInteractionController interactionControllerWithURL:URL];
        self.controller.delegate = self;
        
        [self.controller presentOpenInMenuFromRect:[sender frame] inView:self.view animated:YES];
    }
}

- (void)dealloc
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
