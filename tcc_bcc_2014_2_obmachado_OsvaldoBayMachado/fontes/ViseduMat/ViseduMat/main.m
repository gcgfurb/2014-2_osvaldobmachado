//
//  main.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/10/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
