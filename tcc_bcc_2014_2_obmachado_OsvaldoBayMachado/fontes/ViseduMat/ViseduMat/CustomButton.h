//
//  CustomButton.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 9/30/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomButton : UIButton {
    id userData;
}

@property (nonatomic, readwrite, retain) id userData;

@end
