//
//  ObjetoCena.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/22/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Utils.h"

@interface ObjetoCena : NSObject

@property (nonatomic, strong) NSString* identificador;
@property (nonatomic, strong) NSString* funcao;
@property (nonatomic, strong) NSString* contraDominio;
@property (nonatomic, strong) NSNumber* faixaANegativa;
@property (nonatomic, strong) NSNumber* faixaAPositiva;
@property (nonatomic, strong) NSNumber* faixaBNegativa;
@property (nonatomic, strong) NSNumber* faixaBPositiva;
@property (nonatomic, strong) NSNumber* contraDominioNegativo;
@property (nonatomic, strong) NSNumber* contraDominioPositivo;
@property (nonatomic, strong) NSNumber* visivel;
@property (nonatomic, strong) NSNumber* corObjeto;
@property (nonatomic, strong) NSNumber* modoDimensao;
@property (nonatomic, strong) NSNumber* xTracing;
@property (nonatomic, strong) NSNumber* yTracing;
@property (nonatomic) GLuint vboVertices;
@property (nonatomic) GLuint vboIndicesMesh;
@property (nonatomic) GLuint vboIndicesWireframe;
@property (nonatomic) GLuint vboBoundingBox;
@property (nonatomic, strong) NSMutableArray* posicaoVertices;
@property (nonatomic, strong) NSMutableArray* posicaoIndicesMesh;
@property (nonatomic, strong) NSMutableArray* posicaoIndicesWireframe;

- (id)initWithFuncao:(NSString *)f contraDominio:(NSString *)cd modoDimensao:(EnumModo)modo;

@end
