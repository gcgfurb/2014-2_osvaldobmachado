//
//  Utils.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 9/7/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

static const int NIVEL_DETALHE_2D = 601;
static const float ESCALA_2D = 100.0f;

static const int NIVEL_DETALHE = 41;
static const float ESCALA = 15.0f;

typedef enum {
    MODO_2D,
    MODO_3D,
} EnumModo;

typedef enum {
    POINTS,
    MESH,
    WIREFRAME,
} EnumPrimitiva;

enum {
    REDCOLOR,
    GREENCOLOR,
    BLUECOLOR,
    CYANCOLOR,
    YELLOWCOLOR,
    MAGENTACOLOR,
    ORANGECOLOR,
    PURPLECOLOR,
    BROWNCOLOR,
    GRAYCOLOR,
    NUM_CORES
};
int EnumCorObjeto[NUM_CORES];

static const GLKVector4 Cores[10] =
{
    {1.0f, 0.0f, 0.0f, 1.0f}, // redColor
    {0.0f, 1.0f, 0.0f, 1.0f}, // greenColor
    {0.0f, 0.0f, 1.0f, 1.0f}, // blueColor
    {0.0f, 1.0f, 1.0f, 1.0f}, // cyanColor
    {1.0f, 1.0f, 0.0f, 1.0f}, // yellowColor
    {1.0f, 0.0f, 1.0f, 1.0f}, // magentaColor
    {1.0f, 0.5f, 0.0f, 1.0f}, // orangeColor
    {0.5f, 0.0f, 0.5f, 1.0f}, // purpleColor
    {0.6f, 0.4f, 0.2f, 1.0f}, // brownColor
    {0.5f, 0.5f, 0.5f, 1.0f}, // grayColor
};

enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    UNIFORM_COLOR,
    UNIFORM_USE_NORMAL,
    UNIFORM_POINT_SIZE,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

typedef struct {
    float Position[3];
    float Normal[3];
} Vertex;

@interface Utils : NSObject

@end
