//
//  ObjetoCena.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/22/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "ObjetoCena.h"

@implementation ObjetoCena

- (id)initWithFuncao:(NSString *)f contraDominio:(NSString *)cd modoDimensao:(EnumModo)modo
{
    if (self = [super init]) {
        
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        NSString *uuidStr = (__bridge_transfer NSString *)CFUUIDCreateString(NULL, uuid);
        CFRelease(uuid);
        
        self.identificador = uuidStr;
        self.funcao = f;
        self.contraDominio = cd;
        self.faixaANegativa = [NSNumber numberWithFloat:-7.0f];
        self.faixaAPositiva = [NSNumber numberWithFloat:7.0f];
        self.faixaBNegativa = [NSNumber numberWithFloat:-7.0f];
        self.faixaBPositiva = [NSNumber numberWithFloat:7.0f];
        self.contraDominioNegativo = [NSNumber numberWithFloat:0.0f];
        self.contraDominioPositivo = [NSNumber numberWithFloat:0.0f];
        self.visivel = [NSNumber numberWithBool:YES];
        self.corObjeto = [NSNumber numberWithInteger:REDCOLOR];
        self.modoDimensao = [NSNumber numberWithInteger:modo];
        self.xTracing = [NSNumber numberWithFloat:0.0f];
        self.yTracing = [NSNumber numberWithFloat:0.0f];
        self.vboVertices = 0;
        self.vboIndicesMesh = 0;
        self.vboIndicesWireframe = 0;
        self.vboBoundingBox = 0;
        self.posicaoVertices = [[NSMutableArray alloc] init];
        self.posicaoIndicesMesh = [[NSMutableArray alloc] init];
        self.posicaoIndicesWireframe = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
