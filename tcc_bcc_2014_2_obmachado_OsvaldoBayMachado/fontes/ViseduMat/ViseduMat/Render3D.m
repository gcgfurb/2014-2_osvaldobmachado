//
//  Render3D.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 10/11/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "Render3D.h"

@implementation Render3D

- (void)carregaDesenhoFuncao3D
{
    if (self.objetosCena != nil && [self.objetosCena count] > 0) {
        
        ObjetoCena *objetoCena;
        
        for (int i = 0; i < [self.objetosCena count]; i++) {
            
            objetoCena = [self.objetosCena objectAtIndex:i];
            
            if (objetoCena.posicaoVertices != nil && [objetoCena.posicaoVertices count] > 0) {
                
                NSUInteger tamanhoPosicaoVertices = [objetoCena.posicaoVertices count];
                
                if (tamanhoPosicaoVertices > 0) {
                    
                    GLfloat fPosicaoVertices[tamanhoPosicaoVertices];
                    
                    for (int posVtx = 0; posVtx < tamanhoPosicaoVertices; posVtx++)
                        fPosicaoVertices[posVtx] = [[objetoCena.posicaoVertices objectAtIndex:posVtx] floatValue];
                    
                    if (objetoCena.vboVertices == 0) {
                        GLuint vboVertices;
                        glGenBuffers(1, &vboVertices);
                        objetoCena.vboVertices = vboVertices;
                    }
                    
                    glBindBuffer(GL_ARRAY_BUFFER, objetoCena.vboVertices);
                    glBufferData(GL_ARRAY_BUFFER, tamanhoPosicaoVertices * sizeof(float), fPosicaoVertices, GL_STATIC_DRAW);
                    
                    if (objetoCena.posicaoIndicesMesh != nil && [objetoCena.posicaoIndicesMesh count] > 0) {
                        
                        NSUInteger tamanhoPosicaoIndicesMesh = [objetoCena.posicaoIndicesMesh count];
                        
                        if (tamanhoPosicaoIndicesMesh > 0) {
                            
                            int iPosicaoIndicesMesh[tamanhoPosicaoIndicesMesh];
                            
                            for (int posIdx = 0; posIdx < tamanhoPosicaoIndicesMesh; posIdx++)
                                iPosicaoIndicesMesh[posIdx] = [[objetoCena.posicaoIndicesMesh objectAtIndex:posIdx] intValue];
                            
                            if (objetoCena.vboIndicesMesh == 0) {
                                GLuint vboIndicesMesh;
                                glGenBuffers(1, &vboIndicesMesh);
                                objetoCena.vboIndicesMesh = vboIndicesMesh;
                            }
                            
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, objetoCena.vboIndicesMesh);
                            glBufferData(GL_ELEMENT_ARRAY_BUFFER, tamanhoPosicaoIndicesMesh * sizeof(int), iPosicaoIndicesMesh, GL_STATIC_DRAW);
                        }
                    }
                    
                    if (objetoCena.posicaoIndicesWireframe != nil && [objetoCena.posicaoIndicesWireframe count] > 0) {
                        
                        NSUInteger tamanhoPosicaoIndicesWireframe = [objetoCena.posicaoIndicesWireframe count];
                        
                        if (tamanhoPosicaoIndicesWireframe > 0) {
                            
                            int iPosicaoIndicesWireframe[tamanhoPosicaoIndicesWireframe];
                            
                            for (int posIdx = 0; posIdx < tamanhoPosicaoIndicesWireframe; posIdx++)
                                iPosicaoIndicesWireframe[posIdx] = [[objetoCena.posicaoIndicesWireframe objectAtIndex:posIdx] intValue];
                            
                            if (objetoCena.vboIndicesWireframe == 0) {
                                GLuint vboIndicesWireframe;
                                glGenBuffers(1, &vboIndicesWireframe);
                                objetoCena.vboIndicesWireframe = vboIndicesWireframe;
                            }
                            
                            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, objetoCena.vboIndicesWireframe);
                            glBufferData(GL_ELEMENT_ARRAY_BUFFER, tamanhoPosicaoIndicesWireframe * sizeof(int), iPosicaoIndicesWireframe, GL_STATIC_DRAW);
                        }
                    }
                    
                }
                
                [self carregaBoundingBoxWithObjetoCena:objetoCena];
            }
            
            [objetoCena.posicaoVertices removeAllObjects];
            [objetoCena.posicaoIndicesMesh removeAllObjects];
            [objetoCena.posicaoIndicesWireframe removeAllObjects];
        }
    }
}

- (void)mostraDesenhoFuncao3DWithPrimitiva:(EnumPrimitiva)primitiva
{
    if (self.objetosCena != nil && [self.objetosCena count] > 0) {
        ObjetoCena *objetoCena;
        
        for (int i = 0; i < [self.objetosCena count]; i++) {
            objetoCena = [self.objetosCena objectAtIndex:i];
            
            if ([objetoCena.visivel boolValue]) {
                
                GLuint vboVertices = objetoCena.vboVertices;
                GLuint vboIndicesMesh = objetoCena.vboIndicesMesh;
                GLuint vboIndicesWireframe = objetoCena.vboIndicesWireframe;
                
                GLKVector4 corObjeto = Cores[[objetoCena.corObjeto integerValue]];
                
                glUniform1i(uniforms[UNIFORM_USE_NORMAL], 1);
                glUniform4fv(uniforms[UNIFORM_COLOR], 1, corObjeto.v);
                
                glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
                
                glEnableVertexAttribArray(GLKVertexAttribPosition);
                glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid *) offsetof(Vertex, Position));
                
                glEnableVertexAttribArray(GLKVertexAttribNormal);
                glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid *) offsetof(Vertex, Normal));
                
                if (primitiva == POINTS)
                {
                    glUniform1f(uniforms[UNIFORM_POINT_SIZE], 3.0f);
                    NSUInteger tamanhoVtx = NIVEL_DETALHE * NIVEL_DETALHE;
                    glDrawArrays(GL_POINTS, 0, (int)tamanhoVtx);
                }
                else if (primitiva == MESH)
                {
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndicesMesh);
                    NSUInteger tamanhoIdx = (NIVEL_DETALHE-1) * (NIVEL_DETALHE-1) * 6;
                    glDrawElements(GL_TRIANGLES, (int)tamanhoIdx, GL_UNSIGNED_INT, 0);
                }
                else
                {
                    glLineWidth(1.0f);
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndicesWireframe);
                    NSUInteger tamanhoIdx = NIVEL_DETALHE * (NIVEL_DETALHE-1) * 4;
                    glDrawElements(GL_LINES, (int)tamanhoIdx, GL_UNSIGNED_INT, 0);
                }
                
                if (self.objetoCenaSelecionado != nil) {
                    if ([[self.objetoCenaSelecionado identificador] isEqualToString:[objetoCena identificador]])
                    {
                        [self mostraBoundingBoxWithObjetoCena:objetoCena];
                    }
                }
                
            }
        }
    }
    
}

- (void)carregaBoundingBoxWithObjetoCena:(ObjetoCena *)objeto
{
    NSString *contraDominio = objeto.contraDominio;
    float xMin = 0.0f;
    float yMin = 0.0f;
    float zMin = 0.0f;
    float xMax = 0.0f;
    float yMax = 0.0f;
    float zMax = 0.0f;
    
    // contraDominio | faixaA | faixaB
    if ([contraDominio isEqual: @"x"]) {
        
        xMin = [objeto.contraDominioNegativo floatValue];
        yMin = [objeto.faixaANegativa floatValue];
        zMin = [objeto.faixaBNegativa floatValue];
        xMax = [objeto.contraDominioPositivo floatValue];
        yMax = [objeto.faixaAPositiva floatValue];
        zMax = [objeto.faixaBPositiva floatValue];
        
    // faixaA | contraDominio | faixaB
    }else if ([contraDominio isEqual: @"y"]) {
        
        xMin = [objeto.faixaANegativa floatValue];
        yMin = [objeto.contraDominioNegativo floatValue];
        zMin = [objeto.faixaBNegativa floatValue];
        xMax = [objeto.faixaAPositiva floatValue];
        yMax = [objeto.contraDominioPositivo floatValue];
        zMax = [objeto.faixaBPositiva floatValue];
        
    // faixaA | faixaB | contraDominio
    }else if ([contraDominio isEqual: @"z"]) {
        
        xMin = [objeto.faixaANegativa floatValue];
        yMin = [objeto.faixaBNegativa floatValue];
        zMin = [objeto.contraDominioNegativo floatValue];
        xMax = [objeto.faixaAPositiva floatValue];
        yMax = [objeto.faixaBPositiva floatValue];
        zMax = [objeto.contraDominioPositivo floatValue];
        
    }
    
    GLfloat boundingBox[264] = {
        
        // face frontal
        xMin, yMin, zMax,
        xMax, yMin, zMax,
        xMax, yMin, zMax,
        xMax, yMax, zMax,
        xMax, yMax, zMax,
        xMin, yMax, zMax,
        xMin, yMax, zMax,
        xMin, yMin, zMax,
        
        // face traseira
        xMin, yMin, zMin,
        xMax, yMin, zMin,
        xMax, yMin, zMin,
        xMax, yMax, zMin,
        xMax, yMax, zMin,
        xMin, yMax, zMin,
        xMin, yMax, zMin,
        xMin, yMin, zMin,
        
        // face lateral esquerda
        xMin, yMin, zMax,
        xMin, yMin, zMin,
        xMin, yMin, zMin,
        xMin, yMax, zMin,
        xMin, yMax, zMin,
        xMin, yMax, zMax,
        xMin, yMax, zMax,
        xMin, yMin, zMax,
        
        // face lateral direita
        xMax, yMin, zMax,
        xMax, yMin, zMin,
        xMax, yMin, zMin,
        xMax, yMax, zMin,
        xMax, yMax, zMin,
        xMax, yMax, zMax,
        xMax, yMax, zMax,
        xMax, yMin, zMax,
        
        // face superior
        xMin, yMax, zMax,
        xMax, yMax, zMax,
        xMax, yMax, zMax,
        xMax, yMax, zMin,
        xMax, yMax, zMin,
        xMin, yMax, zMin,
        xMin, yMax, zMin,
        xMin, yMax, zMax,
        
        // face inferior
        xMin, yMin, zMax,
        xMax, yMin, zMax,
        xMax, yMin, zMax,
        xMax, yMin, zMin,
        xMax, yMin, zMin,
        xMin, yMin, zMin,
        xMin, yMin, zMin,
        xMin, yMin, zMax,
        
    };
    
    if (objeto.vboBoundingBox == 0) {
        GLuint vboBoundingBox;
        glGenBuffers(1, &vboBoundingBox);
        objeto.vboBoundingBox = vboBoundingBox;
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, objeto.vboBoundingBox);
    glBufferData(GL_ARRAY_BUFFER, sizeof(boundingBox), boundingBox, GL_STATIC_DRAW);
    
}

- (void)mostraBoundingBoxWithObjetoCena:(ObjetoCena *)objeto
{
    GLuint vboBoundingBox = objeto.vboBoundingBox;
    
    glLineWidth(5.0f);
    
    // Eixo X
    glBindBuffer(GL_ARRAY_BUFFER, vboBoundingBox);
    
    GLfloat corEixoX[4] = {0.0f, 0.0f, 1.0f, 1.0f};
    glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoX);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
    
    glDrawArrays(GL_LINES, 0, 264);
}

@end
