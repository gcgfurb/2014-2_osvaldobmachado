//
//  Shader.vsh
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/8/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

attribute vec4 position;
attribute vec3 normal;

varying lowp vec4 colorVarying;

uniform mat4 modelViewProjectionMatrix;
uniform vec4 color;
uniform int useNormal;
uniform float pointSize;

void main()
{
    if (useNormal > 0) {
        vec3 eyeNormal = normalize(normal);
        vec3 lightPosition = vec3(1.0, 1.0, 1.0);
        
        float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));
        
        colorVarying = color * nDotVP;
    }
    else
    {
        colorVarying = color;
    }
    
    gl_Position = modelViewProjectionMatrix * position;
    gl_PointSize = pointSize;
}