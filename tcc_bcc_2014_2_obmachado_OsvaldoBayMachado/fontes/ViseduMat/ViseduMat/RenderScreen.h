//
//  RenderScreen.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/18/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "Transformations.h"
#import "ObjetoCena.h"
#import "ParserMath.h"
#import "Render2D.h"
#import "Render3D.h"
#import "Utils.h"

@interface RenderScreen : NSObject

@property (weak, nonatomic) ObjetoCena *objetoCenaSelecionado;
@property (weak, nonatomic) NSMutableArray *objetosCena;
@property (nonatomic) EnumPrimitiva primitiva;
@property (nonatomic) EnumModo modo;

- (id)initWithContext:(EAGLContext *)c View:(UIView *)v Transformation:(id)t Primitiva:(EnumPrimitiva)p;
- (void)setupGL;
- (void)update;
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect;
- (void)tearDownGL;
- (void)desenhaFuncoes;
- (void)desenhaPontoTracing:(NSNumber *)exibir;

@end
