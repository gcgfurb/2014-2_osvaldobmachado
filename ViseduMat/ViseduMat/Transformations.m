//
//  Transformations.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/11/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "Transformations.h"

@interface Transformations ()
{
    // 1
    // Depth
    float   _depth;
    
    // Scale
    float   _scaleStart;
    float   _scaleEnd;
    
    // Translation
    GLKVector2  _translationStart;
    GLKVector2  _translationEnd;
    
    // Rotation
    GLKVector3      _rotationStart;
    GLKQuaternion   _rotationEnd;
    // Vectors
    GLKVector3      _front;
    
    // Rotation Two Finger
    GLKVector3      _right;
    GLKVector3      _up;
}

@end

@implementation Transformations

- (id)initWithDepth:(float)z Scale:(float)s Translation:(GLKVector2)t Rotation:(GLKVector3)r
{
    if(self = [super init])
    {
        // 2
        // Depth
        _depth = z;
        
        // Scale
        _scaleEnd = s;
        
        // Translation
        _translationEnd = t;
        
        // Rotate
        _front = GLKVector3Make(0.0f, 0.0f, 1.0f);
        
        // Rotate Two Finger
        _right = GLKVector3Make(1.0f, 0.0f, 0.0f);
        _up = GLKVector3Make(0.0f, 1.0f, 0.0f);
        r.x = GLKMathDegreesToRadians(r.x);
        r.y = GLKMathDegreesToRadians(r.y);
        
        // Rotate
        r.z = GLKMathDegreesToRadians(r.z);
        
        _rotationEnd = GLKQuaternionIdentity;
        
        // Rotate Two Finger
        _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(-r.x, _right),
                                             _rotationEnd);
        _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(-r.y, _up),
                                             _rotationEnd);
        // Rotate
        _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(-r.z,_front),
                                             _rotationEnd);
    }
    
    return self;
}

- (void)start
{
    _scaleStart = _scaleEnd;
    _translationStart = GLKVector2Make(0.0f, 0.0f);
    _rotationStart = GLKVector3Make(0.0f, 0.0f, 0.0f);
    
    // Locking Your Gestures/Transformations
    self.state = S_NEW;
}

- (void)scale:(float)s
{
    float result = s * _scaleStart;
    
    if (result < 2.0f && result > 0.05f) {
        
        self.state = S_SCALE;
        _scaleEnd = s * _scaleStart;
        
    }

}

- (void)translate:(GLKVector2)t withMultiplier:(float)m
{
    self.state = S_TRANSLATION;
    
    // 1
    t = GLKVector2MultiplyScalar(t, m);
    
    // 2
    float dx = _translationEnd.x + (t.x-_translationStart.x);
    float dy = _translationEnd.y - (t.y-_translationStart.y);
    
    // 3
    _translationEnd = GLKVector2Make(dx, dy);
    _translationStart = GLKVector2Make(t.x, t.y);
}

- (void)rotate:(GLKVector3)r withMultiplier:(float)m
{
    self.state = S_ROTATION;
    
    //Rotation Two Finger
    float dx = r.x - _rotationStart.x;
    float dy = r.y - _rotationStart.y;
    //Rotation
    float dz = r.z - _rotationStart.z;
    
    _rotationStart = GLKVector3Make(r.x, r.y, r.z);
    
    //Rotation Two Finger
    _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(dx*m, _up),
                                         _rotationEnd);
    _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(dy*m, _right),
                                         _rotationEnd);
    //Rotation
    _rotationEnd = GLKQuaternionMultiply(GLKQuaternionMakeWithAngleAndVector3Axis(-dz, _front),
                                         _rotationEnd);
}

// You position your model-view matrix at the (x,y) center of your view with the values (0.0, 0.0) and
// with a z-value of -_depth. You do this because, in OpenGL ES, the negative z-axis runs into the screen.
- (GLKMatrix4)getModelViewMatrix
{
    // 3
    //Translation
    GLKMatrix4 modelViewMatrix = GLKMatrix4Identity;
    
    //Rotation
    GLKMatrix4 quaternionMatrix = GLKMatrix4MakeWithQuaternion(_rotationEnd);
    
    //Translation
    modelViewMatrix = GLKMatrix4Translate(modelViewMatrix, _translationEnd.x, _translationEnd.y, -_depth);
    
    //Rotation
    modelViewMatrix = GLKMatrix4Multiply(modelViewMatrix, quaternionMatrix);
    
    //Scale = you scale your model-view matrix uniformly in (x,y,z) space.
    modelViewMatrix = GLKMatrix4Scale(modelViewMatrix, _scaleEnd, _scaleEnd, _scaleEnd);
    
    return modelViewMatrix;
}

@end
