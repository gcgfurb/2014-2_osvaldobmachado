//
//  LibraryViewController.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 11/16/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utils.h"

@interface LibraryViewController : UIViewController <UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) UIDocumentInteractionController *controller;

- (id)initWithModo:(EnumModo)m;

@end
