//
//  Render3D.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 10/11/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RenderScreen.h"

@interface Render3D : NSObject

@property (weak, nonatomic) NSMutableArray *objetosCena;
@property (weak, nonatomic) ObjetoCena *objetoCenaSelecionado;

- (void)carregaDesenhoFuncao3D;
- (void)mostraDesenhoFuncao3DWithPrimitiva:(EnumPrimitiva)primitiva;

@end
