//
//  Shader.fsh
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/8/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
