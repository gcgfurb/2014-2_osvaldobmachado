//
//  ParserMath.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/25/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "ParserMath.h"

@implementation ParserMath

- (NSString *)geraTracing2DWithObjetoCena:(ObjetoCena *)objeto Faixa:(float)faixa
{
    NSString *expression = objeto.funcao;
    //NSLog(@"%@", expression);
    
    NSError *error = nil;
    
    DDMathOperatorSet *operator = nil;
    DDMathStringTokenizer *tokenizer = [[DDMathStringTokenizer alloc] initWithString:expression operatorSet:operator error:&error];
    
    if (error != nil) {
        return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
    }
    
    NSArray *tokens = [tokenizer allObjects];
    tokens = [tokens valueForKey:@"token"];
    //NSLog(@"%@", tokens);
    
    NSMutableString *analize = [[NSMutableString alloc] initWithString:@""];
    
    for (NSInteger i = 0; i < [tokens count]; i++) {
        if ([tokens[i] isEqual:@"x"] || [tokens[i] isEqual:@"y"]) {
            [analize appendString:@"$"];
            [analize appendString:tokens[i]];
            i = i + 2;
        }else{
            [analize appendString:tokens[i]];
        }
    }
    
    //NSLog(@"%@", analize);
    
    NSMutableString *result = [[NSMutableString alloc] initWithString:@""];
    
    [objeto.posicaoVertices removeAllObjects];
    
    float x = 0.0f;
    float y = 0.0f;
    
    NSDictionary *dictionary;
        
    if ([objeto.contraDominio isEqualToString:@"x"]) {
        
        if (faixa < 0) {
            y = faixa * ([objeto.faixaANegativa floatValue] * - 1.0f);
        } else {
            y = faixa * [objeto.faixaAPositiva floatValue];
        }
        
        dictionary = [NSDictionary dictionaryWithObjects:
                      [NSArray arrayWithObjects:[NSNumber numberWithFloat: y], nil]
                                                 forKeys:[NSArray arrayWithObjects:@"y", nil]];
        
        DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
        x = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
    }
    else if ([objeto.contraDominio isEqualToString:@"y"]) {
        
        if (faixa < 0) {
            x = faixa * ([objeto.faixaANegativa floatValue] * - 1.0f);
        } else {
            x = faixa * [objeto.faixaAPositiva floatValue];
        }
        
        dictionary = [NSDictionary dictionaryWithObjects:
                      [NSArray arrayWithObjects:[NSNumber numberWithFloat: x], nil]
                                                 forKeys:[NSArray arrayWithObjects:@"x", nil]];
        
        DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
        y = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
    }
    
    if (error != nil) {
        return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
    }
    
    [result appendString:[NSString stringWithFormat:@" x = %f", x]];
    [result appendString:[NSString stringWithFormat:@" y = %f", y]];
    [result appendString:@"\n"];
    
    [objeto setXTracing:[NSNumber numberWithFloat:x]];
    [objeto setYTracing:[NSNumber numberWithFloat:y]];
    
    //NSLog(@"%@", result);
    
    return nil;
}

- (NSString *)geraPontos2DWithObjetoCena:(ObjetoCena *)objeto
{
    NSString *expression = objeto.funcao;
    //NSLog(@"%@", expression);
    
    NSError *error = nil;
    
    DDMathOperatorSet *operator = nil;
    DDMathStringTokenizer *tokenizer = [[DDMathStringTokenizer alloc] initWithString:expression operatorSet:operator error:&error];
    
    if (error != nil) {
        return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
    }
    
    NSArray *tokens = [tokenizer allObjects];
    tokens = [tokens valueForKey:@"token"];
    //NSLog(@"%@", tokens);
    
    NSMutableString *analize = [[NSMutableString alloc] initWithString:@""];
    
    for (NSInteger i = 0; i < [tokens count]; i++) {
        if ([tokens[i] isEqual:@"x"] || [tokens[i] isEqual:@"y"]) {
            [analize appendString:@"$"];
            [analize appendString:tokens[i]];
            i = i + 2;
        }else{
            [analize appendString:tokens[i]];
        }
    }
    
    //NSLog(@"%@", analize);
    
    NSMutableString *result = [[NSMutableString alloc] initWithString:@""];
    
    [objeto.posicaoVertices removeAllObjects];
    
    float x = 0.0f;
    float y = 0.0f;
    
    float posicaoA = 0.0f;
    
    NSDictionary *dictionary;
    
    for (int eixoA = -NIVEL_DETALHE_2D/2; eixoA <= NIVEL_DETALHE_2D/2; eixoA++ )
    {
        if(eixoA < 0)
            posicaoA = ((float) eixoA / (NIVEL_DETALHE_2D/2)) * ([objeto.faixaANegativa floatValue] * -1.0f);
        else
            posicaoA = ((float) eixoA / (NIVEL_DETALHE_2D/2)) * [objeto.faixaAPositiva floatValue];
        
        
        if ([objeto.contraDominio isEqualToString:@"x"]) {
            
            y = posicaoA;
            
            dictionary = [NSDictionary dictionaryWithObjects:
                          [NSArray arrayWithObjects:[NSNumber numberWithFloat: y], nil]
                                                     forKeys:[NSArray arrayWithObjects:@"y", nil]];
            
            DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
            x = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
        }
        else if ([objeto.contraDominio isEqualToString:@"y"]) {
            
            x = posicaoA;
            
            dictionary = [NSDictionary dictionaryWithObjects:
                          [NSArray arrayWithObjects:[NSNumber numberWithFloat: x], nil]
                                                     forKeys:[NSArray arrayWithObjects:@"x", nil]];
            
            DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
            y = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
        }
        
        if (error != nil) {
            return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
        }
        
        [result appendString:[NSString stringWithFormat:@" x = %f", x]];
        [result appendString:[NSString stringWithFormat:@" y = %f", y]];
        [result appendString:@"\n"];
        
        [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:x]];
        [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:y]];
    }
    
    //NSLog(@"%@", result);
    
    return nil;
    
}

- (NSString *)geraPontos3DWithObjetoCena:(ObjetoCena *)objeto
{
    NSString *expression = objeto.funcao;
    NSError *error = nil;
    
    DDMathStringTokenizer *tokenizer = [[DDMathStringTokenizer alloc] initWithString:expression operatorSet:nil error:&error];
    
    if (error != nil) {
        return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
    }
    
    NSArray *tokens = [tokenizer allObjects];
    tokens = [tokens valueForKey:@"token"];
    
    NSMutableString *analize = [[NSMutableString alloc] initWithString:@""];

    for (NSInteger i = 0; i < [tokens count]; i++) {
        if ([tokens[i] isEqual:@"x"] || [tokens[i] isEqual:@"y"] || [tokens[i] isEqual:@"z"]) {
            [analize appendString:@"$"];
            [analize appendString:tokens[i]];
            i = i + 2;
        }else{
            [analize appendString:tokens[i]];
        }
    }

    //NSLog(@"%@", expression);
    //NSLog(@"%@", tokens);
    //NSLog(@"%@", analize);

    NSMutableString *result = [[NSMutableString alloc] initWithString:@""];

    [objeto.posicaoVertices removeAllObjects];
    [objeto.posicaoIndicesMesh removeAllObjects];
    [objeto.posicaoIndicesWireframe removeAllObjects];

    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

    float posicaoA = 0.0f;
    float posicaoB = 0.0f;

    NSDictionary *dictionary;
    
    float valorContraDominio = 0.0f;

    for (int eixoA = -NIVEL_DETALHE/2; eixoA <= NIVEL_DETALHE/2; eixoA++ )
    {
        for (int eixoB = -NIVEL_DETALHE/2; eixoB <= NIVEL_DETALHE/2; eixoB++ )
        {
            
            if(eixoA < 0)
                posicaoA = ((float) eixoA / (NIVEL_DETALHE/2)) * ([objeto.faixaANegativa floatValue] * -1.0f);
            else
                posicaoA = ((float) eixoA / (NIVEL_DETALHE/2)) * [objeto.faixaAPositiva floatValue];
            
            if(eixoB < 0)
                posicaoB = ((float) eixoB / (NIVEL_DETALHE/2)) * ([objeto.faixaBNegativa floatValue] * -1.0f);
            else
                posicaoB = ((float) eixoB / (NIVEL_DETALHE/2)) * [objeto.faixaBPositiva floatValue];
            
            
            if ([objeto.contraDominio isEqualToString:@"x"]) {
                
                y = posicaoA;
                z = posicaoB;
                
                dictionary = [NSDictionary dictionaryWithObjects:
                              [NSArray arrayWithObjects:[NSNumber numberWithFloat: y],[NSNumber numberWithFloat: z], nil]
                                                         forKeys:[NSArray arrayWithObjects:@"y", @"z", nil]];
                
                DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
                x = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
                valorContraDominio = x;
            }
            else if ([objeto.contraDominio isEqualToString:@"y"]) {
                
                x = posicaoA;
                z = posicaoB;
                
                dictionary = [NSDictionary dictionaryWithObjects:
                              [NSArray arrayWithObjects:[NSNumber numberWithFloat: x],[NSNumber numberWithFloat: z], nil]
                                                         forKeys:[NSArray arrayWithObjects:@"x", @"z", nil]];
                
                DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
                y = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
                valorContraDominio = y;
            }
            else if ([objeto.contraDominio isEqualToString:@"z"]) {
                
                x = posicaoA;
                y = posicaoB;
                
                dictionary = [NSDictionary dictionaryWithObjects:
                              [NSArray arrayWithObjects:[NSNumber numberWithFloat: x],[NSNumber numberWithFloat: y], nil]
                                                         forKeys:[NSArray arrayWithObjects:@"x", @"y", nil]];
                
                DDMathEvaluator *eval = [DDMathEvaluator defaultMathEvaluator];
                z = [[eval evaluateString:analize withSubstitutions:dictionary error:&error] floatValue];
                valorContraDominio = z;
            }
            
            if (error != nil) {
                return [NSString stringWithFormat:@"%s", [[error localizedDescription] UTF8String]];
            }
            
            [result appendString:[NSString stringWithFormat:@" x = %f", x]];
            [result appendString:[NSString stringWithFormat:@" y = %f", y]];
            [result appendString:[NSString stringWithFormat:@" z = %f", z]];
            [result appendString:@"\n"];
            
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:x]];
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:y]];
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:z]];
            
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:powf(x, 2.0f)]];
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:powf(y, 2.0f)]];
            [objeto.posicaoVertices addObject: [NSNumber numberWithFloat:powf(z, 2.0f)]];
            
            if ([objeto.contraDominioNegativo floatValue] == 0) {
                objeto.contraDominioNegativo = [NSNumber numberWithFloat:valorContraDominio];
            }
            
            if ([objeto.contraDominioPositivo floatValue] == 0) {
                objeto.contraDominioPositivo = [NSNumber numberWithFloat:valorContraDominio];
            }
            
            if ([objeto.contraDominioNegativo floatValue] > valorContraDominio) {
                objeto.contraDominioNegativo = [NSNumber numberWithFloat:valorContraDominio];
            }
            if ([objeto.contraDominioPositivo floatValue] < valorContraDominio) {
                objeto.contraDominioPositivo = [NSNumber numberWithFloat:valorContraDominio];
            }
            
        }
    }

    // GL_TRIANGLES
    for (int a = 0; a < NIVEL_DETALHE-1; a++ )
    {
        for (int b = 0; b < NIVEL_DETALHE-1; b++ )
        {
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(( a + 1 ) * NIVEL_DETALHE) + b]];
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(a * NIVEL_DETALHE) + b + 1]];
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(a * NIVEL_DETALHE) + b]];
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(( a + 1 ) * NIVEL_DETALHE) + b]];
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(( a + 1 ) * NIVEL_DETALHE) + b + 1]];
            [objeto.posicaoIndicesMesh addObject: [NSNumber numberWithInt:(a * NIVEL_DETALHE) + b + 1]];
        }
    }

    // GL_LINES
    for (int a = 0; a < NIVEL_DETALHE; a++ )
    {
        for (int b = 0; b < NIVEL_DETALHE-1; b++ )
        {
            [objeto.posicaoIndicesWireframe addObject: [NSNumber numberWithInt:((a * NIVEL_DETALHE) + b)]];
            [objeto.posicaoIndicesWireframe addObject: [NSNumber numberWithInt:((a * NIVEL_DETALHE) + b + 1)]];
            [objeto.posicaoIndicesWireframe addObject: [NSNumber numberWithInt:((b * NIVEL_DETALHE) + a)]];
            [objeto.posicaoIndicesWireframe addObject: [NSNumber numberWithInt:((b * NIVEL_DETALHE) + a + NIVEL_DETALHE)]];
        }
    }

    //NSLog(@"%@", result);
    
    return nil;
    
}
@end