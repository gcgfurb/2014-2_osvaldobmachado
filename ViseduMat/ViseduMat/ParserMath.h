//
//  ParserMath.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/25/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DDMathParser.h"
#import "DDMathStringTokenizer.h"
#import "DDMathOperator.h"
#import "ObjetoCena.h"
#import "Utils.h"

@interface ParserMath : NSObject

- (NSString *)geraTracing2DWithObjetoCena:(ObjetoCena *)objeto Faixa:(float)faixa;
- (NSString *)geraPontos2DWithObjetoCena:(ObjetoCena *)objeto;
- (NSString *)geraPontos3DWithObjetoCena:(ObjetoCena *)objeto;

@end
