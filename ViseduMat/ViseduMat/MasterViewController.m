//
//  MasterViewController.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/10/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()
{
    NSNumber *botaoModoPressionado;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@property (strong, nonatomic) ParserMath *parserMath;
@property (strong, nonatomic) NSNumber *tracingVisivel;

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self inicializa];
}

- (void)inicializa
{
    botaoModoPressionado = [NSNumber numberWithBool:NO];
    self.modo = MODO_3D;
    self.primitiva = MESH;
    self.navigationItem.title = @"3D";
    
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.detailViewController.masterViewController = self;
    
    self.parserMath = [[ParserMath alloc] init];
    
    [self carregaFuncoes];
    
    [self inicializaBarraBotoes];
    
    [self inicializaEventos];
    
    self.ativaTracingButton.hidden = YES;
    self.tracingSlider.hidden = YES;
    self.xTracingLabel.hidden = YES;
    self.yTracingLabel.hidden = YES;
    self.tracingVisivel = [NSNumber numberWithBool:YES];
}

- (void)inicializaEventos
{
    [self.contraDominioButton addTarget:self action:@selector(alteraContraDominio:)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.adicionarButton addTarget:self action:@selector(insereObjetoCena:)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.faixaANegativaSlider addTarget:self action:@selector(selecionandoFaixaANegativa:)
                        forControlEvents:UIControlEventAllEvents];
    [self.faixaAPositivaSlider addTarget:self action:@selector(selecionandoFaixaAPositiva:)
                        forControlEvents:UIControlEventAllEvents];
    [self.faixaBNegativaSlider addTarget:self action:@selector(selecionandoFaixaBNegativa:)
                        forControlEvents:UIControlEventAllEvents];
    [self.faixaBPositivaSlider addTarget:self action:@selector(selecionandoFaixaBPositiva:)
                        forControlEvents:UIControlEventAllEvents];
    
    [self.faixaANegativaSlider addTarget:self action:@selector(mudaFaixaANegativa:)
                        forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [self.faixaAPositivaSlider addTarget:self action:@selector(mudaFaixaAPositiva:)
                        forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [self.faixaBNegativaSlider addTarget:self action:@selector(mudaFaixaBNegativa:)
                        forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [self.faixaBPositivaSlider addTarget:self action:@selector(mudaFaixaBPositiva:)
                        forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    
    [self.tracingSlider addTarget:self action:@selector(selecionandoTracing:)
                 forControlEvents:UIControlEventAllEvents];
    [self.ativaTracingButton addTarget:self action:@selector(alteraAtivaTracing) forControlEvents:UIControlEventTouchUpInside];
}

- (void)inicializaBarraBotoes
{
    // Biblioteca de funcoes
    
    UIButton *btBiblioteca = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btBiblioteca.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    
    [btBiblioteca addTarget:self action:@selector(exibirBiblioteca) forControlEvents:UIControlEventTouchDown];
    UIImage *imgBiblioteca = [[UIImage imageNamed:@"biblioteca.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [btBiblioteca setImage:imgBiblioteca forState:UIControlStateNormal];
    
    UIView *vBiblioteca=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f) ];
    
    [vBiblioteca addSubview:btBiblioteca];
    
    UIBarButtonItem *uiBarItemBiblioteca = [[UIBarButtonItem alloc] initWithCustomView:vBiblioteca];
    
    
    // Muda primitiva (mesh, points, wireframe)
    
    UIButton *btPrimitiva = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btPrimitiva.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    
    [btPrimitiva addTarget:self action:@selector(alterarPrimitiva) forControlEvents:UIControlEventTouchDown];
    UIImage *imgPrimitiva = [[UIImage imageNamed:@"primitiva.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [btPrimitiva setImage:imgPrimitiva forState:UIControlStateNormal];
    
    UIView *vPrimitiva=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f) ];
    
    [vPrimitiva addSubview:btPrimitiva];
    
    UIBarButtonItem *uiBarItemPrimitiva = [[UIBarButtonItem alloc] initWithCustomView:vPrimitiva];
    
    
    // Adiciona botao biblioteca e primitiva
    
    NSMutableArray *arrayBarItemLeft= [[NSMutableArray alloc]init];
    [arrayBarItemLeft addObject:uiBarItemBiblioteca];
    [arrayBarItemLeft addObject:uiBarItemPrimitiva];
    
    self.navigationItem.leftBarButtonItems = arrayBarItemLeft;
    
    
    // Modo 2D ou 3D
    
    UIButton *btDimensao = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btDimensao.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    
    [btDimensao addTarget:self action:@selector(alterarDimensao:) forControlEvents:UIControlEventTouchDown];
    UIImage *imgDimensao = [[UIImage imageNamed:@"dimensao.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [btDimensao setImage:imgDimensao forState:UIControlStateNormal];
    
    UIView *vDimensao=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f) ];
    
    [vDimensao addSubview:btDimensao];
    
    UIBarButtonItem *uiBarItemDimensao = [[UIBarButtonItem alloc] initWithCustomView:vDimensao];
    
    
    // Centraliza camera
    
    UIButton *btCamera = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btCamera.frame = CGRectMake(0.0f, 0.0f, 40.0f, 40.0f);
    
    [btCamera addTarget:self action:@selector(alterarCamera) forControlEvents:UIControlEventTouchDown];
    UIImage *imgCamera = [[UIImage imageNamed:@"camera.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [btCamera setImage:imgCamera forState:UIControlStateNormal];
    
    UIView *vCamera=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f) ];
    
    [vCamera addSubview:btCamera];
    
    UIBarButtonItem *uiBarItemCamera = [[UIBarButtonItem alloc] initWithCustomView:vCamera];
    
    
    // Adiciona botao modo e centraliza
    
    NSMutableArray *arrayBarItemRight = [[NSMutableArray alloc]init];
    [arrayBarItemRight addObject:uiBarItemDimensao];
    [arrayBarItemRight addObject:uiBarItemCamera];
    
    self.navigationItem.rightBarButtonItems = arrayBarItemRight;
}

- (void) alteraAtivaTracing
{
    if ([self.tracingVisivel boolValue] == NO) {
        self.tracingVisivel = [NSNumber numberWithBool:YES];
    }else{
        self.tracingVisivel = [NSNumber numberWithBool:NO];
    }
    
    [self.detailViewController exibeTracing2D:self.tracingVisivel];
    
}

- (void)selecionandoTracing:(UISlider *)sender
{
    if (self.modo == MODO_2D && [self.tracingVisivel boolValue] == YES) {
        if (self.objetoCenaSelecionado != nil) {
            ObjetoCena *objeto = self.objetoCenaSelecionado;
            float valueSlider = sender.value;
            
            [self.parserMath geraTracing2DWithObjetoCena:objeto Faixa:valueSlider];
            
            [self.xTracingLabel setText:[NSString stringWithFormat:@"x = %.1f", [objeto.xTracing floatValue]]];
            [self.yTracingLabel setText:[NSString stringWithFormat:@"y = %.1f", [objeto.yTracing floatValue]]];
            
            [self.detailViewController exibeTracing2D:self.tracingVisivel];
        }
    }
}

- (void)atualizaTracing
{
    if (self.modo == MODO_2D) {
        if (self.objetoCenaSelecionado != nil) {
            ObjetoCena *objeto = self.objetoCenaSelecionado;
            NSString *contraDominio = objeto.contraDominio;
            
            [self.parserMath geraTracing2DWithObjetoCena:objeto Faixa:0.0f];
            
            if ([contraDominio isEqual: @"x"]) {
                [self.tracingSlider setValue:[objeto.yTracing floatValue]];
            }else if ([contraDominio isEqual: @"y"]) {
                [self.tracingSlider setValue:[objeto.xTracing floatValue]];
            }
            
            [self.xTracingLabel setText:[NSString stringWithFormat:@"x = %.1f", [objeto.xTracing floatValue]]];
            [self.yTracingLabel setText:[NSString stringWithFormat:@"y = %.1f", [objeto.yTracing floatValue]]];
            
            [self.detailViewController exibeTracing2D:self.tracingVisivel];
        }
    }
}

- (void) carregaFuncoes
{
    if (self.arrayObjetoCena == nil) {
        self.arrayObjetoCena = [[NSMutableArray alloc] init];
    }
    else {
        [self.arrayObjetoCena removeAllObjects];
    }
     
    EnumModo modoDimensao = self.modo;
    
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"ObjetoCena"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"modoDimensao = '%d'", (int)modoDimensao]];
    [request setPredicate:predicate];
    NSArray* objects = [self.managedObjectContext executeFetchRequest:request error:NULL];
    
    
    for (int i = 0; i < [objects count]; i++) {
        
        ObjetoCena *objetoBancoDados = objects[i];
        
        ObjetoCena *objeto = [[ObjetoCena alloc]initWithFuncao:objetoBancoDados.funcao contraDominio:objetoBancoDados.contraDominio modoDimensao:self.modo];
        
        objeto.visivel = objetoBancoDados.visivel;
        objeto.corObjeto = objetoBancoDados.corObjeto;
        objeto.modoDimensao = objetoBancoDados.modoDimensao;
        objeto.faixaANegativa = objetoBancoDados.faixaANegativa;
        objeto.faixaAPositiva = objetoBancoDados.faixaAPositiva;
        objeto.faixaBNegativa = objetoBancoDados.faixaBNegativa;
        objeto.faixaBPositiva = objetoBancoDados.faixaBPositiva;
        objeto.identificador = objetoBancoDados.identificador;
        
        if (self.modo == MODO_2D) {
            [self.parserMath geraPontos2DWithObjetoCena:objeto];
        }else {
            [self.parserMath geraPontos3DWithObjetoCena:objeto];
        }
        
        [self.arrayObjetoCena addObject:objeto];
        
    }
    
    [self.tableView reloadData];
}

- (void) exibirBiblioteca
{
    LibraryViewController *viewController = [[LibraryViewController alloc] initWithModo:self.modo];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void) alterarPrimitiva
{
    if (self.modo == MODO_3D)
    {
        switch (self.primitiva) {
            case POINTS:
                self.primitiva  = MESH;
                break;
            case MESH:
                self.primitiva  = WIREFRAME;
                break;
            case WIREFRAME:
                self.primitiva  = POINTS;
                break;
        }
        
        [self.detailViewController atualizaCena];
    }
}

- (void) alterarDimensao:(UIButton *)sender
{
    //NSLog(@"alterarDimensao");
    
    if ([botaoModoPressionado boolValue] == NO) {
        
        //NSLog(@"Bloquea");
        botaoModoPressionado = [NSNumber numberWithBool:YES];
        
        switch (self.modo) {
            case MODO_2D:
                self.modo = MODO_3D;
                self.navigationItem.title = @"3D";
                [self alterarCamera];
                
                self.faixaBNegativaLabel.hidden = NO;
                self.faixaBPositivaLabel.hidden = NO;
                self.faixaBNegativaSlider.hidden = NO;
                self.faixaBPositivaSlider.hidden = NO;
                
                self.ativaTracingButton.hidden = YES;
                self.tracingSlider.hidden = YES;
                self.xTracingLabel.hidden = YES;
                self.yTracingLabel.hidden = YES;
                
                break;
            case MODO_3D:
                self.modo = MODO_2D;
                self.navigationItem.title = @"2D";
                [self alterarCamera];
                
                self.faixaBNegativaLabel.hidden = YES;
                self.faixaBPositivaLabel.hidden = YES;
                self.faixaBNegativaSlider.hidden = YES;
                self.faixaBPositivaSlider.hidden = YES;
                
                self.ativaTracingButton.hidden = NO;
                self.tracingSlider.hidden = NO;
                self.xTracingLabel.hidden = NO;
                self.yTracingLabel.hidden = NO;
                
                [self.contraDominioButton setTitle:@"y" forState:UIControlStateNormal];
                
                break;
        }
        
        self.objetoCenaSelecionado = nil;
        [self carregaFuncoes];
        [self.detailViewController atualizaCena];
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //NSLog(@"Libera");
            botaoModoPressionado = [NSNumber numberWithBool:NO];
        });
    }
}

- (void) alterarCamera
{
    [self.detailViewController ajustaOrientacaoCamera];
}

- (void)selecionandoFaixaANegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = sender.value;
        NSString *texto;
        
        if (self.modo == MODO_3D) {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }else if ([contraDominio isEqual: @"z"]) {
                texto = @"x = %.1f";
            }
        } else {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }
        }
        
        objeto.faixaANegativa = [NSNumber numberWithFloat:valueSlider];
        
        [self.faixaANegativaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
    }
}

- (void)mudaFaixaANegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        
        [self alteraObjetoCena:objeto];
        
        if (self.modo == MODO_2D) {
            [self.parserMath geraPontos2DWithObjetoCena:(ObjetoCena *) objeto];
        } else {
            [self.parserMath geraPontos3DWithObjetoCena:(ObjetoCena *) objeto];
        }

        [self.detailViewController atualizaCena];
        [self atualizaTracing];
    }
}

- (void)atualizaFaixaANegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = [objeto.faixaANegativa floatValue];
        NSString *texto;
        
        if (self.modo == MODO_3D) {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }else if ([contraDominio isEqual: @"z"]) {
                texto = @"x = %.1f";
            }
        } else {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }
        }
        
        
        [self.faixaANegativaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
        
        [sender setValue:valueSlider];
    }
}

- (void)selecionandoFaixaAPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = sender.value;
        NSString *texto;
        
        if (self.modo == MODO_3D) {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }else if ([contraDominio isEqual: @"z"]) {
                texto = @"x = %.1f";
            }
        } else {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }
        }
        
        objeto.faixaAPositiva = [NSNumber numberWithFloat:valueSlider];
        
        [self.faixaAPositivaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
    }
}

- (void)mudaFaixaAPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        
        [self alteraObjetoCena:objeto];
        
        if (self.modo == MODO_2D) {
            [self.parserMath geraPontos2DWithObjetoCena:(ObjetoCena *) objeto];
        } else {
            [self.parserMath geraPontos3DWithObjetoCena:(ObjetoCena *) objeto];
        }

        [self.detailViewController atualizaCena];
        [self atualizaTracing];
    }
}

- (void)atualizaFaixaAPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = [objeto.faixaAPositiva floatValue];
        NSString *texto;
        
        if (self.modo == MODO_3D) {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }else if ([contraDominio isEqual: @"z"]) {
                texto = @"x = %.1f";
            }
        } else {
            if ([contraDominio isEqual: @"x"]) {
                texto = @"y = %.1f";
            }else if ([contraDominio isEqual: @"y"]) {
                texto = @"x = %.1f";
            }
        }
        
        [self.faixaAPositivaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
        
        [sender setValue:valueSlider];
    }
}

- (void)selecionandoFaixaBNegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = sender.value;
        NSString *texto;
        
        if ([contraDominio isEqual: @"x"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"y"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"z"]) {
            texto = @"y = %.1f";
        }
        objeto.faixaBNegativa = [NSNumber numberWithFloat:valueSlider];
        
        [self.faixaBNegativaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
    }
}

- (void)mudaFaixaBNegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        
        [self alteraObjetoCena:objeto];
        
        if (self.modo == MODO_2D) {
            [self.parserMath geraPontos2DWithObjetoCena:(ObjetoCena *) objeto];
        } else {
            [self.parserMath geraPontos3DWithObjetoCena:(ObjetoCena *) objeto];
        }

        [self.detailViewController atualizaCena];
    }
}

- (void)atualizaFaixaBNegativa:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = [objeto.faixaBNegativa floatValue];
        NSString *texto;
        
        if ([contraDominio isEqual: @"x"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"y"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"z"]) {
            texto = @"y = %.1f";
        }
        [self.faixaBNegativaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
        
        [sender setValue:valueSlider];
    }
}

- (void)selecionandoFaixaBPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = sender.value;
        NSString *texto;
        
        if ([contraDominio isEqual: @"x"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"y"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"z"]) {
            texto = @"y = %.1f";
        }
        objeto.faixaBPositiva = [NSNumber numberWithFloat:valueSlider];
        
        [self.faixaBPositivaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
    }
}

- (void)mudaFaixaBPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        
        [self alteraObjetoCena:objeto];
        
        if (self.modo == MODO_2D) {
            [self.parserMath geraPontos2DWithObjetoCena:(ObjetoCena *) objeto];
        } else {
            [self.parserMath geraPontos3DWithObjetoCena:(ObjetoCena *) objeto];
        }

        [self.detailViewController atualizaCena];
    }
}

- (void)atualizaFaixaBPositiva:(UISlider *)sender
{
    if (self.objetoCenaSelecionado != nil) {
        ObjetoCena *objeto = self.objetoCenaSelecionado;
        NSString *contraDominio = objeto.contraDominio;
        float valueSlider = [objeto.faixaBPositiva floatValue];
        NSString *texto;
        
        if ([contraDominio isEqual: @"x"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"y"]) {
            texto = @"z = %.1f";
        }else if ([contraDominio isEqual: @"z"]) {
            texto = @"y = %.1f";
        }
        [self.faixaBPositivaLabel setText:[NSString stringWithFormat:texto, valueSlider]];
        
        [sender setValue:valueSlider];
    }
}

- (void)alteraContraDominio:(id)sender
{
    UIButton *button = (UIButton *) sender;
    
    if (self.modo == MODO_3D) {
        
        if ([button.titleLabel.text isEqual: @"x"]) {
            [button setTitle:@"y" forState:UIControlStateNormal];
        }else if ([button.titleLabel.text isEqual: @"y"]) {
            [button setTitle:@"z" forState:UIControlStateNormal];
        }else if ([button.titleLabel.text isEqual: @"z"]) {
            [button setTitle:@"x" forState:UIControlStateNormal];
        }
        
    } else {
        
        if ([button.titleLabel.text isEqual: @"x"]) {
            [button setTitle:@"y" forState:UIControlStateNormal];
        }else if ([button.titleLabel.text isEqual: @"y"]) {
            [button setTitle:@"x" forState:UIControlStateNormal];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insereObjetoCena:(id)sender
{
    if (![[self.funcaoTextField text] isEqual:@""])
    {
        ObjetoCena *objeto = [[ObjetoCena alloc]initWithFuncao:[self.funcaoTextField text] contraDominio:self.contraDominioButton.titleLabel.text modoDimensao:self.modo];
        
        NSString *erro = nil;
        
        if (self.modo == MODO_2D) {
            erro = [self.parserMath geraPontos2DWithObjetoCena:(ObjetoCena *) objeto];
        } else {
            erro = [self.parserMath geraPontos3DWithObjetoCena:(ObjetoCena *) objeto];
        }
        
        if (erro != nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao inserir a função"
                                                            message:erro
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else {
            
            if ([self salvarObjetoCena:objeto] == YES) {
                
                [self.arrayObjetoCena addObject:objeto];
                [self.tableView reloadData];
                [self.funcaoTextField setText:@""];
                [self.detailViewController atualizaCena];
                
            }
            else
            {
                abort();
            }
        }
    }
}

-(BOOL)salvarObjetoCena:(ObjetoCena *) objeto
{
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    [newManagedObject setValue:[objeto identificador] forKey:@"identificador"];
    [newManagedObject setValue:[objeto funcao] forKey:@"funcao"];
    [newManagedObject setValue:[objeto contraDominio] forKey:@"contraDominio"];
    [newManagedObject setValue:[objeto faixaANegativa] forKey:@"faixaANegativa"];
    [newManagedObject setValue:[objeto faixaAPositiva] forKey:@"faixaAPositiva"];
    [newManagedObject setValue:[objeto faixaBNegativa] forKey:@"faixaBNegativa"];
    [newManagedObject setValue:[objeto faixaBPositiva] forKey:@"faixaBPositiva"];
    [newManagedObject setValue:[objeto visivel] forKey:@"visivel"];
    [newManagedObject setValue:[objeto corObjeto] forKey:@"corObjeto"];
    [newManagedObject setValue:[objeto modoDimensao] forKey:@"modoDimensao"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    else
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)alteraObjetoCena:(ObjetoCena *) objeto
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ObjetoCena"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"identificador = '%@'", objeto.identificador]];
    [request setPredicate:pred];
    
    NSArray *objetoArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    if ([objetoArray count] > 0){
        ObjetoCena *objetoCena = objetoArray[0];
        objetoCena.visivel = objeto.visivel;
        objetoCena.corObjeto = objeto.corObjeto;
        objetoCena.modoDimensao = objeto.modoDimensao;
        objetoCena.faixaANegativa = objeto.faixaANegativa;
        objetoCena.faixaAPositiva = objeto.faixaAPositiva;
        objetoCena.faixaBNegativa = objeto.faixaBNegativa;
        objetoCena.faixaBPositiva = objeto.faixaBPositiva;
        objetoCena.identificador = objeto.identificador;
        objetoCena.funcao = objeto.funcao;
        objetoCena.contraDominio = objeto.contraDominio;
        
        // Save the context.
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        else{
            return YES;
        }
    }
    return NO;
}

-(BOOL)deleteObjetoCena:(ObjetoCena *) objeto
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ObjetoCena"
                                              inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"identificador = '%@'", objeto.identificador]];
    [request setPredicate:pred];
    
    NSArray *objetoArray=[self.managedObjectContext executeFetchRequest:request error:nil];
    
    if ([objetoArray count] > 0){
        [self.managedObjectContext deleteObject:objetoArray[0]];
        
        // Save the context.
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }else{
            [self.arrayObjetoCena removeObject:objeto];
            [self.tableView reloadData];
            return YES;
        }
    }
    return NO;
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayObjetoCena count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"eventCell"];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ObjetoCena *objeto = self.arrayObjetoCena[indexPath.row];
    [self deleteObjetoCena:objeto];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.objetoCenaSelecionado = self.arrayObjetoCena[indexPath.row];
    
    [self atualizaFaixaANegativa:self.faixaANegativaSlider];
    [self atualizaFaixaAPositiva:self.faixaAPositivaSlider];
    [self atualizaFaixaBNegativa:self.faixaBNegativaSlider];
    [self atualizaFaixaBPositiva:self.faixaBPositivaSlider];
    [self atualizaTracing];
    
    [self.detailViewController atualizaCena];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ObjetoCena" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"identificador" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
//  aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CustomButton *btVisivel = nil;
    UILabel *fromLabel = nil;
    CustomButton *btCor = nil;
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    cell.textLabel.text = @"";
    UIView *uiView = [[UIView alloc] init];
    uiView.backgroundColor = [UIColor colorWithRed:0.9f green:0.9f blue:0.9f alpha:1.0f];
    [cell setSelectedBackgroundView:uiView];
    
    
    btVisivel = [CustomButton buttonWithType:UIButtonTypeCustom];
    btVisivel.tag = 13;
    btVisivel.frame = CGRectMake(5.0f, 9.0f, 24.0f, 24.0f);
    [cell.contentView addSubview:btVisivel];
    
    fromLabel = [[UILabel alloc]init];
    fromLabel.tag = 12;
    fromLabel.frame = CGRectMake(55.0f, 0.0f, 210.0f, 44.0f);
    fromLabel.adjustsFontSizeToFitWidth = YES;
    [cell.contentView addSubview:fromLabel];
    
    btCor = [CustomButton buttonWithType:UIButtonTypeRoundedRect];
    btCor.tag = 11;
    btCor.frame = CGRectMake(280.0f, 9.0f, 24.0f, 24.0f);
    [cell.contentView addSubview:btCor];
    
    
    ObjetoCena *objeto = self.arrayObjetoCena[indexPath.row];
    
    UIImage *imgVisivel;
    
    if ([objeto.visivel boolValue] == YES) {
        imgVisivel = [[UIImage imageNamed:@"visivel.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }else{
        imgVisivel = [[UIImage imageNamed:@"invisivel.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    [btVisivel setImage:imgVisivel forState:UIControlStateNormal];
    
    [btVisivel addTarget:self action:@selector(alteraVisibilidade:) forControlEvents:UIControlEventTouchUpInside];
    btVisivel.userData = indexPath;
    
    
    
    fromLabel.text = [NSString stringWithFormat:@"%@ = %@", [objeto contraDominio], [objeto funcao]];
    
    
    [btCor addTarget:self action:@selector(alterarCor:) forControlEvents:UIControlEventTouchDown];
    UIImage *imgCor = [[UIImage imageNamed:@"cor.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [btCor setImage:imgCor forState:UIControlStateNormal];
    
    switch ([objeto.corObjeto intValue]) {
        case REDCOLOR:
            btCor.tintColor = [UIColor redColor];
            break;
        case GREENCOLOR:
            btCor.tintColor = [UIColor greenColor];
            break;
        case BLUECOLOR:
            btCor.tintColor = [UIColor blueColor];
            break;
        case CYANCOLOR:
            btCor.tintColor = [UIColor cyanColor];
            break;
        case YELLOWCOLOR:
            btCor.tintColor = [UIColor yellowColor];
            break;
        case MAGENTACOLOR:
            btCor.tintColor = [UIColor magentaColor];
            break;
        case ORANGECOLOR:
            btCor.tintColor = [UIColor orangeColor];
            break;
        case PURPLECOLOR:
            btCor.tintColor = [UIColor purpleColor];
            break;
        case BROWNCOLOR:
            btCor.tintColor = [UIColor brownColor];
            break;
        case GRAYCOLOR:
            btCor.tintColor = [UIColor grayColor];
            break;
    }

    btCor.userData = indexPath;
}

- (void)alterarCor:(CustomButton *)sender
{
    if (sender != nil && sender.userData != nil) {
        NSIndexPath *indexPath = (NSIndexPath *)sender.userData;
        
        if (indexPath != nil) {
            ObjetoCena *objeto = self.arrayObjetoCena[indexPath.row];
            
            if (objeto != nil) {
                
                int corObjeto = [objeto.corObjeto intValue];
                corObjeto++;
                
                if (corObjeto >= NUM_CORES) {
                    corObjeto = 0;
                }
                objeto.corObjeto = [NSNumber numberWithInt:corObjeto];
                
                [self alteraObjetoCena:objeto];
                [self.tableView reloadData];
                
                [self.detailViewController atualizaCena];
            }
        }
    }
}

- (void)alteraVisibilidade:(CustomButton *)sender
{
    NSLog(@"Invocado metodo alteraVisibilidade:(id)sender");
    
    if (sender != nil && sender.userData != nil) {
        NSIndexPath *indexPath = (NSIndexPath *)sender.userData;
        
        if (indexPath != nil) {
            ObjetoCena *objeto = self.arrayObjetoCena[indexPath.row];
            
            if (objeto != nil) {
                if ([objeto.visivel boolValue] == YES) {
                    objeto.visivel = [NSNumber numberWithBool:NO];
                }
                else{
                    objeto.visivel = [NSNumber numberWithBool:YES];
                }
                
                [self alteraObjetoCena:objeto];
                [self.tableView reloadData];
                
                [self.detailViewController atualizaCena];
            }
        }
    }
}

@end
