//
//  RenderScreen.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/18/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "RenderScreen.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

@interface RenderScreen () {
    GLuint _program;
    GLKMatrix4 _modelViewProjectionMatrix;
    GLuint _vboEixoX_2D;
    GLuint _vboEixoY_2D;
    GLuint _vboEixoX_3D;
    GLuint _vboEixoY_3D;
    GLuint _vboEixoZ_3D;
    GLuint _vboLinhasBase;
    GLuint _vboLinhasVertical;
    GLuint _vboPontoTracing;
}

@property (weak, nonatomic) EAGLContext *context;
@property (weak, nonatomic) UIView *view;
@property (weak, nonatomic) Transformations *transformations;
@property (strong, nonatomic) Render2D *render2D;
@property (strong, nonatomic) Render3D *render3D;
@property (strong, nonatomic) NSNumber *exibirTracing;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end

@implementation RenderScreen

- (id)initWithContext:(EAGLContext *)c View:(UIView *)v Transformation:(id)t Primitiva:(EnumPrimitiva)p
{
    if (self = [super init]) {
        self.context = c;
        self.view = v;
        self.transformations = t;
        self.primitiva = p;
        
        [self loadShaders];
        
        self.render2D = [[Render2D alloc] init];
        self.render3D = [[Render3D alloc] init];
        
        self.exibirTracing = [NSNumber numberWithBool:NO];
        
    }
    return self;
}

- (void)setupGL;
{
    glDisable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    
    [self desenhaEixos3D];
    
    [self desenhaEixos2D];
    
    [self desenhaLinhasBase];
    
    [self desenhaLinhasVertical];
    
    [self desenhaFuncoes];
    
}

- (void)desenhaEixos3D
{
    GLfloat eixoX[6] =
    {
        1 * -ESCALA, 0, 0,
        1 *  ESCALA, 0, 0
    };
    
    glGenBuffers(1, &_vboEixoX_3D);
    glBindBuffer(GL_ARRAY_BUFFER, _vboEixoX_3D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eixoX), eixoX, GL_STATIC_DRAW);
    
    GLfloat eixoY[6] =
    {
        0, 1 * -ESCALA, 0,
        0, 1 *  ESCALA, 0
    };
    
    glGenBuffers(1, &_vboEixoY_3D);
    glBindBuffer(GL_ARRAY_BUFFER, _vboEixoY_3D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eixoY), eixoY, GL_STATIC_DRAW);
    
    GLfloat eixoZ[6] =
    {
        0, 0, 1 * -ESCALA,
        0, 0, 1 *  ESCALA
    };
    
    glGenBuffers(1, &_vboEixoZ_3D);
    glBindBuffer(GL_ARRAY_BUFFER, _vboEixoZ_3D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eixoZ), eixoZ, GL_STATIC_DRAW);
    
}

- (void)desenhaEixos2D
{
    GLfloat eixoX[6] =
    {
        1 * -ESCALA_2D, 0, 0,
        1 *  ESCALA_2D, 0, 0
    };
    
    glGenBuffers(1, &_vboEixoX_2D);
    glBindBuffer(GL_ARRAY_BUFFER, _vboEixoX_2D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eixoX), eixoX, GL_STATIC_DRAW);
    
    GLfloat eixoY[6] =
    {
        0, 1 * -ESCALA_2D, 0,
        0, 1 *  ESCALA_2D, 0
    };
    
    glGenBuffers(1, &_vboEixoY_2D);
    glBindBuffer(GL_ARRAY_BUFFER, _vboEixoY_2D);
    glBufferData(GL_ARRAY_BUFFER, sizeof(eixoY), eixoY, GL_STATIC_DRAW);
    
}

- (void)desenhaLinhasBase
{
    GLfloat linhasBase[((int)ESCALA * 8) * 3];
    
    int indice = 0;
    
    for (int x = -ESCALA; x <= ESCALA; x++) {
        if (x == 0)
            continue;
        linhasBase[indice++] = (float)x;
        linhasBase[indice++] = 0.0f;
        linhasBase[indice++] = -ESCALA;
        linhasBase[indice++] = (float)x;
        linhasBase[indice++] = 0.0f;
        linhasBase[indice++] = ESCALA;
    }
    for (int z = -ESCALA; z <= ESCALA; z++) {
        if (z == 0)
            continue;
        linhasBase[indice++] = -ESCALA;
        linhasBase[indice++] = 0.0f;
        linhasBase[indice++] = (float)z;
        linhasBase[indice++] = ESCALA;
        linhasBase[indice++] = 0.0f;
        linhasBase[indice++] = (float)z;
    }
    
    glGenBuffers(1, &_vboLinhasBase);
    glBindBuffer(GL_ARRAY_BUFFER, _vboLinhasBase);
    glBufferData(GL_ARRAY_BUFFER, sizeof(linhasBase), linhasBase, GL_STATIC_DRAW);
    
}

- (void)desenhaLinhasVertical
{
    GLfloat linhasVertical[((int)ESCALA_2D * 8) * 3];
    
    int indice = 0;
    
    for (int x = -ESCALA_2D; x <= ESCALA_2D; x++) {
        if (x == 0)
            continue;
        linhasVertical[indice++] = (float)x;
        linhasVertical[indice++] = -ESCALA_2D;
        linhasVertical[indice++] = 0.0f;
        linhasVertical[indice++] = (float)x;
        linhasVertical[indice++] = ESCALA_2D;
        linhasVertical[indice++] = 0.0f;
    }
    for (int y = -ESCALA_2D; y <= ESCALA_2D; y++) {
        if (y == 0)
            continue;
        linhasVertical[indice++] = -ESCALA_2D;
        linhasVertical[indice++] = (float)y;
        linhasVertical[indice++] = 0.0f;
        linhasVertical[indice++] = ESCALA_2D;
        linhasVertical[indice++] = (float)y;
        linhasVertical[indice++] = 0.0f;
    }
    
    glGenBuffers(1, &_vboLinhasVertical);
    glBindBuffer(GL_ARRAY_BUFFER, _vboLinhasVertical);
    glBufferData(GL_ARRAY_BUFFER, sizeof(linhasVertical), linhasVertical, GL_STATIC_DRAW);
    
}

- (void)desenhaFuncoes
{
    if (self.modo == MODO_2D) {
        
        [self.render2D setObjetosCena:self.objetosCena];
        [self.render2D carregaDesenhoFuncao2D];
        
    } else {
        
        [self.render3D setObjetosCena:self.objetosCena];
        [self.render3D carregaDesenhoFuncao3D];
        
    }
    
}

- (void)desenhaPontoTracing:(NSNumber *)exibir
{
    [self setExibirTracing:exibir];
    
    if (self.objetoCenaSelecionado != nil && [self.exibirTracing boolValue] == YES) {
        
        GLfloat pontoTracing[3] =
        {
            [self.objetoCenaSelecionado.xTracing floatValue],
            [self.objetoCenaSelecionado.yTracing floatValue],
            0
        };
        
        if (_vboPontoTracing == 0) {
            glGenBuffers(1, &_vboPontoTracing);
        }
        
        glBindBuffer(GL_ARRAY_BUFFER, _vboPontoTracing);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pontoTracing), pontoTracing, GL_STATIC_DRAW);
        
    }
}

- (void)tearDownGL
{
    glDeleteBuffers(1, &_vboEixoX_2D);
    glDeleteBuffers(1, &_vboEixoY_2D);
    glDeleteBuffers(1, &_vboEixoX_3D);
    glDeleteBuffers(1, &_vboEixoY_3D);
    glDeleteBuffers(1, &_vboEixoZ_3D);
    glDeleteBuffers(1, &_vboLinhasBase);
    glDeleteBuffers(1, &_vboLinhasVertical);
    glDeleteBuffers(1, &_vboPontoTracing);
    
    if (self.objetosCena != nil && [self.objetosCena count] > 0) {
        
        ObjetoCena *objetoCena;
        
        for (int i = 0; i < [self.objetosCena count]; i++) {
            
            objetoCena = [self.objetosCena objectAtIndex:i];
            
            GLuint vboVertices = objetoCena.vboVertices;
            GLuint vboIndicesMesh = objetoCena.vboIndicesMesh;
            GLuint vboIndicesWireframe = objetoCena.vboIndicesWireframe;
            GLuint vboBoundingBox = objetoCena.vboBoundingBox;
            
            glDeleteBuffers(1, &vboVertices);
            glDeleteBuffers(1, &vboIndicesMesh);
            glDeleteBuffers(1, &vboIndicesWireframe);
            glDeleteBuffers(1, &vboBoundingBox);
            
        }
    }
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(90.0f), aspect, 0.1f, 10.0f);
    
    _modelViewProjectionMatrix = [self.transformations getModelViewMatrix];
    
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, _modelViewProjectionMatrix);
    
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(_program);
    
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    
    if (self.modo == MODO_3D) {
        
        glLineWidth(2.0f);
        
        // Eixo X
        glBindBuffer(GL_ARRAY_BUFFER, _vboEixoX_3D);
        
        glUniform1i(uniforms[UNIFORM_USE_NORMAL], 0);
        GLfloat corEixoX[4] = {1.0f, 0.0f, 0.0f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoX);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, 2);
        
        
        // // Eixo Y
        glBindBuffer(GL_ARRAY_BUFFER, _vboEixoY_3D);
        
        glUniform1i(uniforms[UNIFORM_USE_NORMAL], 0);
        GLfloat corEixoY[4] = {0.0f, 1.0f, 0.0f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoY);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, 2);
        
        
        // // Eixo Z
        glBindBuffer(GL_ARRAY_BUFFER, _vboEixoZ_3D);
        
        glUniform1i(uniforms[UNIFORM_USE_NORMAL], 0);
        GLfloat corEixoZ[4] = {0.0f, 0.0f, 1.0f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoZ);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, 2);
        
        
        glLineWidth(1.0f);
    
        // Linhas Base
        glBindBuffer(GL_ARRAY_BUFFER, _vboLinhasBase);
        
        glUniform1i(uniforms[UNIFORM_USE_NORMAL], 0);
        GLfloat corLinhasBase[4] = {0.7f, 0.7f, 0.7f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corLinhasBase);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, (int)ESCALA * 8);
        
    } else {
        
        glLineWidth(2.0f);
        
        // Eixo X
        glBindBuffer(GL_ARRAY_BUFFER, _vboEixoX_2D);
        
        GLfloat corEixoX[4] = {1.0f, 0.0f, 0.0f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoX);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, 2);
        
        
        // // Eixo Y
        glBindBuffer(GL_ARRAY_BUFFER, _vboEixoY_2D);
        
        GLfloat corEixoY[4] = {0.0f, 1.0f, 0.0f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corEixoY);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, 2);
        
        
        glLineWidth(1.0f);
    
        // Linhas Vertical
        glBindBuffer(GL_ARRAY_BUFFER, _vboLinhasVertical);
        
        GLfloat corLinhasVertical[4] = {0.7f, 0.7f, 0.7f, 1.0f};
        glUniform4fv(uniforms[UNIFORM_COLOR], 1, corLinhasVertical);
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
        
        glDrawArrays(GL_LINES, 0, (int)ESCALA_2D * 8);
    
    }
        

    // Funcoes
    
    if (self.modo == MODO_2D) {
        
        [self.render2D setObjetoCenaSelecionado:self.objetoCenaSelecionado];
        [self.render2D setObjetosCena:self.objetosCena];
        [self.render2D mostraDesenhoFuncao2D];
        
    }else{

        [self.render3D setObjetoCenaSelecionado:self.objetoCenaSelecionado];
        [self.render3D setObjetosCena:self.objetosCena];
        [self.render3D mostraDesenhoFuncao3DWithPrimitiva:self.primitiva];
        
    }
    
    // Tracing
    
    if (self.modo == MODO_2D && self.objetoCenaSelecionado != nil && [self.exibirTracing boolValue] == YES) {
    
        if (_vboPontoTracing != 0) {
            
            glUniform1f(uniforms[UNIFORM_POINT_SIZE], 15.0f);
            
            glBindBuffer(GL_ARRAY_BUFFER, _vboPontoTracing);
            
            GLfloat corPontoTracing[4] = {1.0f, 0.0f, 0.0f, 1.0f};
            glUniform4fv(uniforms[UNIFORM_COLOR], 1, corPontoTracing);
            
            glEnableVertexAttribArray(GLKVertexAttribPosition);
            glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), nil);
            
            glDrawArrays(GL_POINTS, 0, 1);
        }
    }

}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    [EAGLContext setCurrentContext:self.context];
    
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    _program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, GLKVertexAttribPosition, "position");
    glBindAttribLocation(_program, GLKVertexAttribNormal, "normal");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_COLOR] = glGetUniformLocation(_program, "color");
    uniforms[UNIFORM_USE_NORMAL] = glGetUniformLocation(_program, "useNormal");
    uniforms[UNIFORM_POINT_SIZE] = glGetUniformLocation(_program, "pointSize");
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end