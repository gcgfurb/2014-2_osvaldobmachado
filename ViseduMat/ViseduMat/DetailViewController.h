//
//  DetailViewController.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/8/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "MasterViewController.h"

@interface DetailViewController : GLKViewController

@property (strong, nonatomic) IBOutlet UIView *coordenada2DView;
@property (strong, nonatomic) IBOutlet UIView *coordenada3DView;
@property (weak, nonatomic) MasterViewController *masterViewController;

-(void)atualizaCena;
-(void)ajustaOrientacaoCamera;
-(void)exibeTracing2D:(NSNumber *)exibir;

@end

