//
//  MasterViewController.h
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/10/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ObjetoCena.h"
#import "CustomButton.h"
#import "ParserMath.h"
#import "LibraryViewController.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) NSMutableArray *arrayObjetoCena;
@property (strong, nonatomic) ObjetoCena *objetoCenaSelecionado;
@property (nonatomic) EnumPrimitiva primitiva;
@property (nonatomic) EnumModo modo;

@property (strong, nonatomic) IBOutlet UIButton *contraDominioButton;
@property (strong, nonatomic) IBOutlet UIButton *adicionarButton;
@property (strong, nonatomic) IBOutlet UITextField *funcaoTextField;

@property (strong, nonatomic) IBOutlet UILabel *faixaANegativaLabel;
@property (strong, nonatomic) IBOutlet UILabel *faixaAPositivaLabel;
@property (strong, nonatomic) IBOutlet UILabel *faixaBNegativaLabel;
@property (strong, nonatomic) IBOutlet UILabel *faixaBPositivaLabel;

@property (strong, nonatomic) IBOutlet UISlider *faixaANegativaSlider;
@property (strong, nonatomic) IBOutlet UISlider *faixaAPositivaSlider;
@property (strong, nonatomic) IBOutlet UISlider *faixaBNegativaSlider;
@property (strong, nonatomic) IBOutlet UISlider *faixaBPositivaSlider;

@property (strong, nonatomic) IBOutlet UIButton *ativaTracingButton;
@property (strong, nonatomic) IBOutlet UISlider *tracingSlider;
@property (strong, nonatomic) IBOutlet UILabel *xTracingLabel;
@property (strong, nonatomic) IBOutlet UILabel *yTracingLabel;



@end
