//
//  DetailViewController.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 8/8/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "DetailViewController.h"
#import "Transformations.h"
#import "RenderScreen.h"

@interface DetailViewController ()

@property (strong, nonatomic) RenderScreen* renderScreen;
@property (strong, nonatomic) Transformations* transformations;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) EAGLContext *context;

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    //NSLog(@"viewDidLoad");
    
    [super viewDidLoad];
    
    //* utilizado depth = 5 pois fica no meio do range de projecao da matriz (0.1 até 10.0),
    //* fazendo com que o modelo fique bem no meio da cena.
    
    // Initialize transformations
    self.transformations = [[Transformations alloc] initWithDepth:5.0f
                                                            Scale:0.3f
                                                      Translation:GLKVector2Make(0.0f, 0.0f)
                                                         Rotation:GLKVector3Make(-20.0f, 10.0f, 0.0f)];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    self.renderScreen = [[RenderScreen alloc] initWithContext:self.context View:self.view Transformation:self.transformations Primitiva:self.masterViewController.primitiva];
    
    
    [self.renderScreen setPrimitiva:self.masterViewController.primitiva];
    
    [self.renderScreen setObjetosCena:self.masterViewController.arrayObjetoCena];
    
    [self.renderScreen setObjetoCenaSelecionado:self.masterViewController.objetoCenaSelecionado];
    
    [self.renderScreen setModo:self.masterViewController.modo];
    
    [self.renderScreen setupGL];
    
    if (self.masterViewController.modo == MODO_3D) {
        self.coordenada3DView.hidden = NO;
        self.coordenada2DView.hidden = YES;
    } else {
        self.coordenada3DView.hidden = YES;
        self.coordenada2DView.hidden = NO;
    }
    
}

-(void)ajustaOrientacaoCamera
{
    if (self.transformations != nil) {
        
        if ([self.masterViewController modo] == MODO_2D){
            self.transformations = [self.transformations initWithDepth:5.0f
                                                                 Scale:0.7f
                                                           Translation:GLKVector2Make(0.0f, 0.0f)
                                                              Rotation:GLKVector3Make(0.0f, 0.0f, 0.0f)];
        }
        else{
            self.transformations = [self.transformations initWithDepth:5.0f
                                                                 Scale:0.3f
                                                           Translation:GLKVector2Make(0.0f, 0.0f)
                                                        Rotation:GLKVector3Make(-20.0f, 10.0f, 0.0f)];
        }
    }
}

- (void)dealloc
{
    [EAGLContext setCurrentContext:self.context];
    [self.renderScreen tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [EAGLContext setCurrentContext:self.context];
        [self.renderScreen tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    [self.renderScreen update];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    [self.renderScreen glkView:view drawInRect:rect];
}

//////////////////////////////////////////////////////////////////////////////////

#pragma mark - Managing the detail item

-(void)atualizaCena
{
    [self.renderScreen setPrimitiva:self.masterViewController.primitiva];
    
    [self.renderScreen setObjetosCena:self.masterViewController.arrayObjetoCena];
    
    [self.renderScreen setObjetoCenaSelecionado:self.masterViewController.objetoCenaSelecionado];
    
    [self.renderScreen setModo:self.masterViewController.modo];
    
    [self.renderScreen desenhaFuncoes];
    
    if (self.masterViewController.modo == MODO_3D) {
        self.coordenada3DView.hidden = NO;
        self.coordenada2DView.hidden = YES;
    } else {
        self.coordenada3DView.hidden = YES;
        self.coordenada2DView.hidden = NO;
    }
    
}

-(void)exibeTracing2D:(NSNumber *)exibir
{
    [self.renderScreen setObjetoCenaSelecionado:self.masterViewController.objetoCenaSelecionado];
    
    [self.renderScreen desenhaPontoTracing:exibir];
}


//////////////////////////////////////////////////////////////////////////////////

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = @"Funções";
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}
// 0.0 ≤ x ≤ +1.0
// 0.0 ≤ y ≤ +1.0
- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    
    if ([self.masterViewController modo] == MODO_3D){
    
        // Pan (1 Finger)
        if((sender.numberOfTouches == 1) &&
           ((self.transformations.state == S_NEW) || (self.transformations.state == S_TRANSLATION)))
        {
            [self translate:sender];
        }
        
        // Pan (2 Fingers)
        else if((sender.numberOfTouches == 2) &&
                ((self.transformations.state == S_NEW) || (self.transformations.state == S_ROTATION)))
        {
            [self rotate:sender];
        }
        
    }
}

// Inicia sempre com 1.0, se zoom in > 1.0, se zoom out < 1.0
// Esse valor é a quantidade de vezes de zoom in ou zoom out.
- (IBAction)pinch:(UIPinchGestureRecognizer *)sender {
    
    // Pinch
    if((self.transformations.state == S_NEW) || (self.transformations.state == S_SCALE))
    {
        [self scale:sender];
    }
    
}

- (void)translate:(UIPanGestureRecognizer *)sender
{
    CGPoint translation = [sender translationInView:sender.view];
    float x = translation.x/sender.view.frame.size.width;
    float y = translation.y/sender.view.frame.size.height;
    [self.transformations translate:GLKVector2Make(x, y) withMultiplier:5.0f];
    
    //NSLog(@"Transformation   x = %f |  y = %f", x, y);
}

- (void)rotate:(UIPanGestureRecognizer *)sender
{
    const float m = GLKMathDegreesToRadians(1.0f);
    CGPoint rotation = [sender translationInView:sender.view];
    [self.transformations rotate:GLKVector3Make(rotation.x, rotation.y, 0.0f) withMultiplier:m];
    
    //NSLog(@"Rotation   x = %f  |  y = %f", rotation.x, rotation.y);
}

- (void)scale:(UIPinchGestureRecognizer *)sender
{
    float scale = [sender scale];
    [self.transformations scale:scale];
    
    //NSLog(@"Scale = %f", scale);
}

// touchesBegan:withEvent: is the first method to respond whenever your iOS device detects
// a touch on the screen, before the gesture recognizers kick in
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Begin transformations
    [self.transformations start];
    
}

//////////////////////////////////////////////////////////////////////////////////

@end
