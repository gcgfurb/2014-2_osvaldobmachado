//
//  Render2D.m
//  ViseduMat
//
//  Created by Osvaldo Bay Machado on 10/11/14.
//  Copyright (c) 2014 Osvaldo Bay Machado. All rights reserved.
//

#import "Render2D.h"

@implementation Render2D

- (void)carregaDesenhoFuncao2D
{
    if (self.objetosCena != nil && [self.objetosCena count] > 0) {
        
        ObjetoCena *objetoCena;
        
        for (int i = 0; i < [self.objetosCena count]; i++) {
            
            objetoCena = [self.objetosCena objectAtIndex:i];
            
            if (objetoCena.posicaoVertices != nil && [objetoCena.posicaoVertices count] > 0) {
                
                NSUInteger tamanhoPosicaoVertices = [objetoCena.posicaoVertices count];
                
                if (tamanhoPosicaoVertices > 0) {
                    
                    GLfloat fPosicaoVertices[tamanhoPosicaoVertices];
                    
                    for (int posVtx = 0; posVtx < tamanhoPosicaoVertices; posVtx++)
                        fPosicaoVertices[posVtx] = [[objetoCena.posicaoVertices objectAtIndex:posVtx] floatValue];
                    
                    if (objetoCena.vboVertices == 0) {
                        GLuint vboVertices;
                        glGenBuffers(1, &vboVertices);
                        objetoCena.vboVertices = vboVertices;
                    }
                    
                    glBindBuffer(GL_ARRAY_BUFFER, objetoCena.vboVertices);
                    glBufferData(GL_ARRAY_BUFFER, tamanhoPosicaoVertices * sizeof(float), fPosicaoVertices, GL_STATIC_DRAW);
                }
            }
            
            [objetoCena.posicaoVertices removeAllObjects];
        }
    }
}

- (void)mostraDesenhoFuncao2D
{
    if (self.objetosCena != nil && [self.objetosCena count] > 0) {
        
        ObjetoCena *objetoCena;
        
        for (int i = 0; i < [self.objetosCena count]; i++) {
            
            objetoCena = [self.objetosCena objectAtIndex:i];
            
            if ([objetoCena.visivel boolValue]) {
                
                GLuint vboVertices = objetoCena.vboVertices;
                
                GLKVector4 corObjeto = Cores[[objetoCena.corObjeto integerValue]];
                
                glUniform4fv(uniforms[UNIFORM_COLOR], 1, corObjeto.v);
                
                glLineWidth(2.0f);
                
                if (self.objetoCenaSelecionado != nil) {
                    if ([[self.objetoCenaSelecionado identificador] isEqualToString:[objetoCena identificador]])
                    {
                        glLineWidth(5.0f);
                    }
                }
                
                glBindBuffer(GL_ARRAY_BUFFER, vboVertices);
                glEnableVertexAttribArray(GLKVertexAttribPosition);
                glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), NULL);
                
                glDrawArrays(GL_LINE_STRIP, 0, NIVEL_DETALHE_2D);
                
            }
        }
    }
}

@end
